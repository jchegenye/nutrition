(function() {

  var app = angular.module('store-nutrition', []);

  /*Custom Directive INSTED of ng-include*/
  app.directive("nutritionDescription", function(){
    return{
      restrict:'E',
      templateUrl:'nutrition-description.html',
    };

  });

  app.directive("nutritionUpdate", function() {
    return {
      restrict: 'E',
      templateUrl: "nutrition-update.html"
    };

  });

  /*Custom Directive INSTED of ng-include USING "A" - Attribute*/
  app.directive("nutritionHealthReport", function(){
    return {
      restrict:'E',
      templateUrl:"nutrition-health-report.html"
    };

  });

  app.directive('nutritionTabs', function() {
    return {
      restrict: 'E',
      templateUrl: 'nutrition-tabs.html',
      controller: function() {
        this.tab = 1;

        this.isSet = function(checkTab) {
          return this.tab === checkTab;
        };

        this.setTab = function(setTab) {
          this.tab = setTab;
        };
      },
      controllerAs: 'tab'
    };
    
  });

  })();