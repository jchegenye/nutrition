(function() {

  //store-nutrition is the dependencies of HIVpatient
  var app = angular.module('HIVpatients', ['store-nutrition']);

  app.controller('StoreController', ['$http', function($http){
    var store = this;
    //initialise empty array,since page will render b4 our data returns from get request
    store.info = [];
    
    $http.get('/hiv-patients.json').success(function(data){
    /*$http.get('https://maps.googleapis.com/maps/api/geocode/json?address=Nairobi').success(function(data){*/

      store.info = data;

    });
    
  }]);

  app.controller('ReviewController', function(){
    /*Set varable "review" to an empty object*/
    this.review = {};

    /*Create a function addReview */
    this.addReview = function($scope){
      this.review.createdOn = Date.now();

      $scope.reviews.push(this.review);
      /*reset form to an empty object after submission*/
      this.review = {};
    };

  });

    /*app.controller('PostController', ['$scope', function($scope) {
      $scope.list = [];
      $scope.text = 'hello';
      $scope.submit = function() {
        if ($scope.text) {
          $scope.list.push(this.text);
          $scope.location = '';
        }
      };
    }]);*/

})();
