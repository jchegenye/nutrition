<!DOCTYPE html>
<html ng-app="HIVpatients">
    <head>
        <title>Nutrition - AngularJs</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
    </head>

    <body ng-controller="StoreController as store">

        <div class="container">
            <div class="row" style="border: 1px solid #F5F5F5; padding: 0px 20px; border-radius: 5px; margin-top:50px;" >
                <header>
                    <h1 class="text-center">HIV Patients</h1>
                    <h5 class="text-center">
                        <i class="text-center" style="letter-spacing: 5px;">
                            Food & Nutrition | Dietary Managment
                        </i>
                    </h5>
                </header>

                <ol ng-repeat="nutrition in store.info" class="list-unstyled">                   
                    <li>
                        <div class="col-md-12">
                            <nutrition-tabs></nutrition-tabs>
                        </div>
                    </li>
                </ol>
 
            </div>       
        </div>
        
    </body>

    <script type="text/javascript" src="angular-1.5.5/angular.min.js"></script>
    <script src="app.js" type="text/javascript"></script>
    <script src="nutrition.js" type="text/javascript"></script>
</html>
